import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'working-hours',
  templateUrl: './working-hours.component.html',
  styleUrls: ['./working-hours.component.scss']
})
export class WorkingHoursComponent implements OnInit {

  @Input() workingHoursFormGroup: FormGroup;

  from: string;
  to: string;
  select: string = 'open';

  constructor() {}

  ngOnInit() {
    const data = JSON.parse(localStorage.getItem('workingHours'));
    if (data && data['program'] === 'open') {
      this.from = data['from'];
      this.to = data['to'];
    } else if (data) {
      this.select = data['program'];
    }
  }

  public saveForm() {
    localStorage.setItem('workingHours', JSON.stringify(this.workingHoursFormGroup.value));
  }

  public resetForm() {
    this.workingHoursFormGroup.reset({
      program: 'open'
    });
    localStorage.removeItem('workingHours');
  }

}

