import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { StepsService } from '../steps.service';

@Component({
  selector: 'filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Input() filtresFormGroup: FormGroup;
  itemList = [];
  selectedItems = [];
  settings = {};

  constructor(private _service: StepsService) {}

  ngOnInit() {
    this.settings = {
      text: 'Select affiliated countries'
    };

    this._service.getFiltersData().subscribe(
      data => {
        this.itemList = data['country'];
      });

    const data = localStorage.getItem('filters');
    if (data) {
      this.selectedItems = JSON.parse(data);
    }
  }

  public saveForm() {
    localStorage.setItem('filters', JSON.stringify(this.selectedItems));
  }

  public resetForm() {
    this.filtresFormGroup.reset();
    localStorage.removeItem('filters');
  }

}
