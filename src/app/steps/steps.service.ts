import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class StepsService {

  constructor(private http: HttpClient) { }

  public getLocationData() {
      return this.http.get('assets/json/location.json');
  }

  public getFiltersData() {
      return this.http.get('assets/json/filters.json');
  }

  public getAdditionsData() {
      return this.http.get('assets/json/additions.json')
  }

}
